# Smart-RSS++ Web

A machine learning enhanced RSS feed manager (written in C++). This is a rough equivalent to my prior projects [Rusty Smart-RSS Web](https://gitlab.com/pawelstrzebonski/rusty-smart-rss-web), [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web), and [Smart-RSS Nim Web](https://gitlab.com/pawelstrzebonski/smart-rss-nim-web/).

*This project is more for my personal C++ practice/education, so it is rather rough code (probably various bugs and bad practices) and it is not advisable to use.*

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript
