{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell rec {
    name = "shell";
    buildInputs = [
        clang
        gnumake
        pkg-config
    ];
    nativeBuildInputs = [
		curl
		tinyxml-2
		httplib
		cereal
    ];
}
