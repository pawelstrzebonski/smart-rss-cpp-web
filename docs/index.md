# Smart-RSS++ Web

Smart-RSS++ Web is a machine learning enhanced RSS feed manager, written in C++.

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript
