#include "libsmartrss.cpp"
#include "frontend.cpp"
#include <filesystem>
#include <cstdlib>
#include <thread>

static
std::atomic < bool >
  on = false;

int
main ()
{
  // Handle app config directory
  std::filesystem::path filepath = std::getenv ("HOME");
  filepath /= ".config";
  filepath /= "smartrsspp";
  if (!std::filesystem::exists (filepath))
    {
      std::cout << "Folder " << filepath << " does not exist, creating" <<
	std::endl;
      std::filesystem::create_directory (filepath);
    }
  // Load from file if file exists
  filepath /= "smartrss.cereal";
  auto backend = LibSmartRss::LibSmartRss ();
  backend.savefile = filepath;
  if (std::filesystem::exists (filepath))
    {
      std::cout << "File " << filepath << " exists, loading" << std::endl;
      backend.read_from_file (&filepath);
    }
  std::cout << "Launching background thread..." << std::endl;
  on = true;			// Signal to run background thread
  std::thread t ([&]()
		 {
		 auto i = 0;
		 while (on)
		 {
		 //NOTE: we have 1 second sleeps and use a counter to enforce update/save times
		 // This is to balance maximizing sleepiness and minimizing latency from exit command to termination
		 if (i % (10 * 60) == 0)
		 {
		 backend.update_feeds ();
		 backend.write_to_file (&backend.savefile);}
		 i++; std::this_thread::sleep_for (std::chrono::seconds (1));}
		 // Write to file on thread termination
		 backend.write_to_file (&backend.savefile);}
  );
  std::cout << "Launched background thread." << std::endl;
  //backend.write_to_file (&filepath);
  //test_functions ();
  frontend::run_server (&backend);
  on = false;			// Signal to end background thread
  t.join ();			// Wait for background thread to end
  return 0;
};
