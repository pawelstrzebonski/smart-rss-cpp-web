#include <httplib.h>
#include "webpage.cpp"

namespace frontend
{

//REF: https://github.com/yhirose/cpp-httplib
  int run_server (LibSmartRss::LibSmartRss *backend)
  {
    using namespace httplib;

		uint limit=50;
		  auto feeds=backend->get_feeds();
		auto items=backend->items_get_unseen_limited(limit);
		  
    Server svr;

      svr.Get ("/",[&](const Request & req, Response & res)
	       {
			   auto output=webpage::index(feeds, items, limit);
	       res.set_content (output, "text/html");}
    );

    svr.Post ("/add",[&](const Request & req, Response & res)
	     {
	     auto url = req.get_param_value ("url");
	     auto num= std::stoi(req.get_param_value ("num"));
	     backend->add_feed(&url, num);
		  feeds=backend->get_feeds();
		items=backend->items_get_unseen_limited(limit);
	     res.status=302;
		res.set_header("Location", "/");
	     res.set_content ("OK", "text/plain");});

    svr.Post ("/remove",[&](const Request & req, Response & res)
	     {
	     auto url = req.get_param_value ("url");
	     backend->remove_feed(&url);
		  feeds=backend->get_feeds();
	     res.status=302;
		res.set_header("Location", "/");
	     res.set_content ("OK", "text/plain");});

    svr.Post ("/setitemnum",[&](const Request & req, Response & res)
	     {
	     auto num= std::stoi(req.get_param_value ("num"));
	     limit=num;
	     res.status=302;
		res.set_header("Location", "/");
	     res.set_content ("OK", "text/plain");});

    svr.Post ("/like",[&](const Request & req, Response & res)
	     {
	     auto url = req.get_param_value ("url");
	     backend->item_mark_liked(&url);
	     res.status=302;
		res.set_header("Location", "/");
	     res.set_content ("OK", "text/plain");});
    svr.Post ("/dislike",[&](const Request & req, Response & res)
	     {
	     auto url = req.get_param_value ("url");
	     backend->item_mark_disliked(&url);
	     res.status=302;
		res.set_header("Location", "/");
	     res.set_content ("OK", "text/plain");});
    svr.Post ("/open",[&](const Request & req, Response & res)
	     {
	     auto url = req.get_param_value ("url");
	     backend->item_mark_opened(&url);
	     res.status=302;
		res.set_header("Location", url);
	     res.set_content ("OK", "text/plain");});
    svr.Post ("/nextpage",[&](const Request & req, Response & res)
	     {
			 for(auto item:items){
				 backend->item_mark_seen(&item->url);
			 }
		items=backend->items_get_unseen_limited(limit);
	     res.status=302;
		res.set_header("Location", "/");
	     res.set_content ("OK", "text/plain");});
	   // Stop the server
	   svr.Post ("/stop",[&](const Request & req, Response & res)
			 {
			 svr.stop ();});
		// Launch server
	     svr.listen ("localhost", 10000); return 0;}
	     }
