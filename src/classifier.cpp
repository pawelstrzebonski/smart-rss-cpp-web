#include <vector>
#include <string>
#include <unordered_map>
#include <cmath>

namespace Classifier
{

// Type alias the string->number hash map
  using map_str_to_int_t = std::unordered_map < std::string, int >;
  using map_str_to_float_t = std::unordered_map < std::string, float >;
  using list_str_t = std::vector < std::string >;

  /*
   * Convert a string to a list of words.
   */
  list_str_t string_to_words (const std::string * document)
  {
    list_str_t words;
    size_t start = 0, end = 0;
    do
      {
	end = document->find (' ', start);
	auto word = document->substr (start, end);
	  start = end + 1;
	  words.push_back (word);
      }
    while (end != std::string::npos);
    return words;
  };

  void increment_map (map_str_to_int_t * map, const std::string * key,
		      const int value)
  {
    if (map->count (key->substr ()) == 0)
      {
	(*map)[key->substr ()] = value;
      }
    else
      {
	(*map)[key->substr ()] += value;
      }
  }

  template < typename K, typename V >
    V get_or_default (std::unordered_map < K, V > *map, const K * key,
		      const V defvalue)
  {
    if (map->count (key->substr ()) == 0)
      {
	return defvalue;
      }
    else
      {
	return (*map)[key->substr ()];
      }
  }
/*
 * Count the occurance of words in a document, returning a hash map
 */
  map_str_to_int_t count_words (const list_str_t * word_list)
  {
    map_str_to_int_t wc;
  for (auto word:*word_list)
      {
	increment_map (&wc, &word, 1);
      }
    return wc;
  }

  class Classifier
  {
  private:
    /// Total # of words encountered
    int total_word_count;
    /// Total # of documents encountered
    int total_document_count;
    /// Total # of documents encountered in a class
    map_str_to_int_t documents_per_class;
    /// Total # of documents of a certain class encountered containing a word
    map_str_to_int_t documents_per_class_word;
    /// Total # of documents encountered containing a word
    map_str_to_int_t documents_per_word;
    /// Total # of word encounters
    map_str_to_int_t word_count;
    /// Total # of words encountered in class
    map_str_to_int_t word_count_per_class;
    /// Per-class/word count
    map_str_to_int_t word_count_per_class_word;
  public:
    /*
     * Train classifier on a document as belonging to given class(es)
     * 
     * docclass should be space separated string of classes
     */
    void add_document (const std::string * document,
		       const std::string * docclasses)
    {
      //TODO: verify, prettify, and document/comment
      // Increment document count
      this->total_document_count++;
      // Count the words
      auto wl = string_to_words (document);
      auto wc = count_words (&wl);
      auto total_words = 0;
      for (auto pair:wc)
	{
	  auto word = pair.first;
	  auto count = pair.second;
	    total_words += count;
	    increment_map (&(this->documents_per_word), &word, 1);
	    increment_map (&(this->word_count), &word, count);
	}
      // Increment document count
      this->total_word_count += total_words;
      auto classlist = string_to_words (docclasses);
    for (auto docclass:classlist)
	{
	  // Increment class count
	  increment_map (&(this->documents_per_class), &docclass, 1);
	  // Increment word counts
	for (auto pair:wc)
	    {
	      auto word = pair.first;
	      auto count = pair.second;
	      auto class_word = docclass + " " + word;
	      increment_map (&(this->documents_per_class_word), &class_word,
			     1);
	      increment_map (&(this->word_count_per_class_word),
			     &class_word, count);
	    }
	  increment_map (&(this->word_count_per_class), &docclass,
			 total_words);
	}
    };
    /*
     * Calculate TF-IDF scores for a list of words.
     * 
     * Will return results as a word->score map.
     */
    map_str_to_float_t tfidf (const std::vector < std::string > *words)
    {
      auto wc = count_words (words);
      map_str_to_float_t scores;
    for (auto pair:wc)
	{
	  auto word = pair.first;
	  auto count = pair.second;
	  float tf = float (count) / float (wc.size ());
	  float idf =
	    std::log (float (this->total_document_count) / (1.0 +
							    float
							    (get_or_default
							     (&
							      (this->
							       documents_per_word),
&word, 0))));
	  scores[word] = tf * idf;
	}
      return scores;
    };
    /*
     * Calculate Naive-Bayes scores for a list of words.
     * 
     * Will return results as a word->score map.
     */
    map_str_to_float_t naivebayes (const std::vector < std::string > *words)
    {
      auto wc = count_words (words);
      map_str_to_float_t p_word, p_class, p_word_given_class,
	p_class_given_word;
    for (auto pair:this->documents_per_class)
	{
	  // Class probability based on fraction of all documents within this class
	  //NOTE: Avoid dividing by zero by clamping denominator to >=1
	  auto docclass = pair.first;
	  auto count = pair.second;
	  p_class[docclass] =
	    float (count) / float (std::max (1, this->total_document_count));
	}
    for (auto pair:wc)
	{
	  // Word probability as fraction of all words
	  //NOTE: Avoid dividing by zero by clamping denominator to >=1
	  auto word = pair.first;
	  p_word[word] =
	    float (get_or_default (&(this->word_count), &word, 0)) /
	    float (std::max (1, this->total_word_count));
	for (auto pair:this->documents_per_class)
	    {
	      // Conditional probability of word given class (fraction of word within the class)
	      //NOTE: Avoid dividing by zero by clamping denominator to >=1
	      auto docclass = pair.first;
	      auto key = docclass + ' ' + word;
	      float value =
		float (get_or_default
		       (&(this->word_count_per_class_word), &key,
			0)) / float (std::max (1,
					       get_or_default
					       (&(this->word_count_per_class),
						&docclass, 1)));
	      p_word_given_class[key] = value;
	    }
	}
    for (auto pair:p_word_given_class)
	{
	  // Bayesian estimation of probability of class given a particular word
	  //NOTE: do some hacks to avoid issues from division by 0
	  auto key = pair.first;
	  auto space_loc = key.find (' ');
	  auto docclass = key.substr (0, space_loc);
	  auto word = key.substr (space_loc + 1);
	  float value = p_class[docclass] * p_word_given_class[key] /
	    p_word[word];
	  if (p_word[word] == 0)
	    {
	      value = 0;
	    }
	  p_class_given_word[key] = value;
	}
      return p_class_given_word;
    };

/*
 * Create a per-class score given list of words
 */
    map_str_to_float_t score (const std::string * document)
    {
		auto words=string_to_words(document);
      map_str_to_float_t scores;
      // Calculate TF-IDF and Naive-Bayes estimations
      auto t = this->tfidf (&words);
      auto n = this->naivebayes (&words);
    for (auto pairs:this->documents_per_class)
	{
	  auto docclass = pairs.first;
	  float class_score = 0;
	for (auto pairs2:t)
	    {
	      auto word = pairs2.first;
	      float word_score = pairs2.second;
	      auto key = docclass + " " + word;
	      // Get NB score for this class given this word
	      float nb_score = get_or_default (&n, &key, float (0));
	      // Add log of NB score (instead of multiplying probabilities, add logs)
	      // and weigh this by the TF-IDF score of this word
	      if (nb_score > 0)
		{
		  class_score += word_score * std::log (nb_score);
		}
	      // And save the total score for this category
	      scores[docclass] = class_score;
	    }
	}
      return scores;
    }

    /*
     * Print the contents of classifier (for debugging)
     */
    void debug_data_dump ()
    {
      std::cout << std::endl;
      std::cout <<
	"========= Printing Classifier Debug Information ===========" <<
	std::endl;
      std::cout << std::endl;
      std::cout << "Total document count: " << this->total_document_count <<
	std::endl;
      std::cout << "Total word count: " << this->total_word_count << std::
	endl;
      std::cout << "Document count (per class): " << std::endl;
    for (auto pairs:this->documents_per_class)
	{
	  std::cout << pairs.first << ": " << pairs.second << std::endl;
	}
      std::cout << "Document count (per word): " << std::endl;
    for (auto pairs:this->documents_per_word)
	{
	  std::cout << pairs.first << ": " << pairs.second << std::endl;
	}
      std::cout << "Document count (per class/word): " << std::endl;
    for (auto pairs:this->documents_per_class_word)
	{
	  std::cout << pairs.first << ": " << pairs.second << std::endl;
	}
      std::cout << "Word count (per class): " << std::endl;
    for (auto pairs:this->word_count_per_class)
	{
	  std::cout << pairs.first << ": " << pairs.second << std::endl;
	}
      std::cout << "Word count (overall): " << std::endl;
    for (auto pairs:this->word_count)
	{
	  std::cout << pairs.first << ": " << pairs.second << std::endl;
	}
      std::cout << "Word count (per class/word): " << std::endl;
    for (auto pairs:this->word_count_per_class_word)
	{
	  std::cout << pairs.first << ": " << pairs.second << std::endl;
	}
      std::cout << std::endl;
      std::cout << "========= End of Classifier Debug Information ==========="
	<< std::endl;
      std::cout << std::endl;
    };
  };

}
