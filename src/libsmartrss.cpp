#include <vector>
#include <string>
#include <chrono>
#include <unordered_map>
#include "http_get.cpp"
#include "tinyxml2.h"
#include "logger.cpp"
#include "classifier.cpp"
#include <regex>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/chrono.hpp>
#include <fstream>
#include <mutex>
#include <filesystem>


namespace LibSmartRss
{
  // define and turn off for the rest of the test suite
  loglevel_e loglevel = logINFO;

  // Define categories
    std::string OPENED = "opened";
    std::string LIKED = "liked";
    std::string DISLIKED = "disliked";

// Regex sanitization

    std::regex REGEX1 ("&.*?;");
    std::regex REGEX2 ("<.*?>");
    std::regex REGEX3 ("[\n\t]");
    std::regex REGEX4 ("[^a-z ]");
    std::string sanitize (const std::string instring)
  {
    // Remove HTML tags/formatting and newlines
    auto outstring = std::regex_replace (instring, REGEX1, " ");
      outstring = std::regex_replace (outstring, REGEX2, " ");
      outstring = std::regex_replace (outstring, REGEX3, " ");
      return outstring;
  }


  /*
   * Data for an RSS feed
   * 
   * Stores title/url for reference/presentation.
   * 
   * Stores time and duration for update scheduling.
   */
  struct Feed
  {
    std::string title, url;
    std::chrono::duration < float >update_period;
      std::chrono::time_point < std::chrono::system_clock > last_update;

      template < class Archive > void serialize (Archive & ar)
    {
      ar (title, url, update_period, last_update);
    }
  };

/*
 * Data for an RSS item
 * 
 * Store key item information (title/description/tags and URL)
 * 
 * Also contains user feedback information (whether it is seen/liked/disliked)
 * 
 * Score for prioritizing items
 * 
 * Time information stored for retention enforcement
 */
  struct Item
  {
    std::string title, description, url, tags;
    float score;
    bool is_seen, is_liked, is_disliked, is_opened;
      std::chrono::time_point < std::chrono::system_clock > update_time;

      template < class Archive > void serialize (Archive & ar)
    {
      ar (title, description, url, tags, score, is_seen, is_liked,
	  is_disliked, is_opened, update_time);
    }
  };

  /*
   * Data type for a scored item (URL-score pair)
   */
  struct ScoredItem
  {
    std::string url;
    float score;
      template < class Archive > void serialize (Archive & ar)
    {
      ar (url, score);
    }
  };

/*
 * Return a list of words for a feed Item
 */
  std::string item_to_input (std::shared_ptr < Item > item)
  {
    // Pull relevant strings from item
    auto big_string =
      item->title + " " + item->description + " " + item->description;
    // Convert to lower case
    std::transform (big_string.cbegin (), big_string.cend (),
		    big_string.begin (),[](unsigned char c)
		    { return std::tolower (c);
		    }
    );
    // Take only a-z characters (and space)
    big_string = std::regex_replace (big_string, REGEX4, "");
    return big_string;
  }

/*
 * Compare scores of scored items (for sorting)
 */
  bool compare_scoreditem (const std::shared_ptr < ScoredItem > i1,
			   const std::shared_ptr < ScoredItem > i2)
  {
    return (i1->score > i2->score);
  }

/*
 * Parse an XML string into a `Feed` struct
 */
  std::shared_ptr < Feed > parse_feed (const std::string feed_xml)
  {
    // Parse XML
    // REF: https://github.com/leethomason/tinyxml2
    logger (logDEBUG) << "Parsing feed XML";
    auto source = &feed_xml.front ();
    tinyxml2::XMLDocument doc;
    doc.Parse (source);
    logger (logDEBUG) << "Parsed XML";
    // Create new feed struct
    std::shared_ptr < Feed > new_feed (new Feed);
    // Pull data from XML and insert into struct
    logger (logDEBUG) << "Extracting feed data from XML";
    auto feed_node = doc.FirstChildElement ("rss");
    // If 'rss' is found, then this is an RSS feed, otherwise it is likely Atom
    if (feed_node != NULL)
      {
	logger (logDEBUG) << "Feed is RSS...";
	new_feed->title =
	  feed_node->FirstChildElement ("channel")->
	  FirstChildElement ("title")->GetText ();
      }
    else
      {
	logger (logDEBUG) << "Feed is Atom...";
	new_feed->title =
	  doc.FirstChildElement ("feed")->
	  FirstChildElement ("title")->GetText ();
      }
    logger (logDEBUG) << "Extracted feed data from XML: " << new_feed->title;
    return new_feed;
  }

/*
 * Parse an XML string into a list of `Item` structs
 */
  std::vector < std::shared_ptr < Item >
    >parse_items (const std::string feed_xml)
  {
    // Parse XML
    // REF: https://github.com/leethomason/tinyxml2
    logger (logDEBUG) << "Parsing feed XML";
    auto source = &feed_xml.front ();
    tinyxml2::XMLDocument doc;
    doc.Parse (source);
    logger (logDEBUG) << "Parsed XML";
    // Create new list of items
    std::vector < std::shared_ptr < Item > >items;
    //REF: https://github.com/leethomason/tinyxml2/blob/master/tinyxml2.cpp
    auto item_key = "item";
    auto content_key = "description";
    auto is_atom = false;
    auto feed = doc.FirstChildElement ("rss");
    // If we found 'rss', then RSS feed, otherwise likely Atom
    if (feed != NULL)
      {
	feed =
	  feed->FirstChildElement ("channel")->FirstChildElement ("item");
	logger (logDEBUG) << "Found RSS channel";
      }
    else
      {
	feed = doc.FirstChildElement ("feed")->FirstChildElement ("entry");
	item_key = "entry";
	content_key = "content";
	logger (logDEBUG) << "Found Atom channel";
	is_atom = true;
      }
    // Iterate over all items
    while (feed)
      {
	// Allocate and fill up new item
	std::shared_ptr < Item > new_item (new Item);
	new_item->title =
	  sanitize (feed->FirstChildElement ("title")->GetText ());
	new_item->description =
	  sanitize (feed->FirstChildElement (content_key)->GetText ());
	if (is_atom)
	  {
	    new_item->url =
	      feed->FirstChildElement ("link")->Attribute ("href");
	  }
	else
	  {
	    new_item->url = feed->FirstChildElement ("link")->GetText ();
	  }
	new_item->update_time = std::chrono::system_clock::now ();
	new_item->is_seen = false;
	new_item->is_disliked = false;
	new_item->is_liked = false;
	new_item->is_opened = false;
	logger (logDEBUG) << "Parsed item " << new_item->title;
	// Add to list of items
	items.push_back (new_item);
	feed = feed->NextSiblingElement (item_key);
      }
    return items;
  }

/*
 * Primary class of this project, representing the backend
 */
  class LibSmartRss
  {
  private:
    std::unordered_map < std::string, std::shared_ptr < Feed > >feeds;
    std::unordered_map < std::string, std::shared_ptr < Item > >items;
    std::vector < std::shared_ptr < ScoredItem > >itemslist;
    Classifier::Classifier classifier;
    bool is_modified;
      std::mutex mutex;
  public:
      std::filesystem::path savefile;

    /*
     * Add a feed given its URL
     * 
     * Return value is boolean indicating whether a feed
     * was added (or ignored due to being added previously)
     */
    bool add_feed (const std::string * url, const uint update_seconds)
    {
      // Quick check to see whether feed was previously added
      if (this->feeds.count (*url) != 0)
	{
	  logger (logINFO) << "Feed " << *url << " already added, skipping";
	  return false;
	};
      logger (logDEBUG) << "Adding feed " << *url;
      // Get the feed as a string
      CURLplusplus client;
      std::string feed_xml = client.Get (*url);
      logger (logDEBUG) << "Got feed " << *url;
      auto feed = parse_feed (feed_xml);
      // Add the URL
      feed->url = *url;
      // Add an update period
      feed->update_period = std::chrono::seconds (update_seconds);
      this->update_feed (*url);
      feed->last_update = std::chrono::system_clock::now ();
      // Add feed to list of feeds
      {
	std::lock_guard < std::mutex > guard (this->mutex);
	this->feeds[*url] = feed;
	this->is_modified = true;
      }
      return true;
    };

    /*
     * Add items for a given feed URL
     * 
     * Return value is boolean indicating whether it finished
     * successfully. (NOTE: no foreseen case for this to be False)
     */
    bool update_feed (const std::string url)
    {
      // Get the feed as a string
      CURLplusplus client;
      std::string feed_xml = client.Get (url);
      // Parse feed into items
      auto new_items = parse_items (feed_xml);
      int added = 0;
    for (auto item:new_items)
	{
	  if (this->items.count (item->url) == 0)
	    {
	      std::lock_guard < std::mutex > guard (this->mutex);
	      // If item is novel, add it to database
	      auto score = this->score_item (item);
	      item->score = score;
	      this->items[item->url] = item;
	      std::shared_ptr < ScoredItem > si (new ScoredItem);
	      si->url = item->url;
	      si->score = score;
	      this->itemslist.push_back (si);
	      this->is_modified = true;
	      added++;
	    }
	  else
	    {
	      // If item is a repeat, delete it
	      //TODO: will this work fine as-is?
	      //delete item;
	    };
	};
      if (added == 0)
	{
	  logger (logDEBUG) << "No new items added for this feed";
	}
      else
	{
	  // Sort list of items
	  std::lock_guard < std::mutex > guard (this->mutex);
	  std::sort (this->itemslist.begin (), this->itemslist.end (),
		     compare_scoreditem);
	}
      return true;
    };

    /*
     * Update all feeds that are due for an update
     * 
     * Return value is boolean indicating whether it finished
     * successfully. (NOTE: no foreseen case for this to be False)
     */
    bool update_feeds ()
    {
      //Get current time
      auto now = std::chrono::system_clock::now ();
      //Iterate over feeds and check if they are due for update
      int updated = 0;
    for (auto pair:this->feeds)
	{
	  auto url = pair.first;
	  auto feed = pair.second;
	  //Verify if needs to be updated
	  if (now - feed->last_update > feed->update_period)
	    {
	      logger (logDEBUG) << "Feed " << url << " due for update";
	      this->update_feed (url);
	      feed->last_update = now;
	      this->is_modified = true;
	      updated++;
	    }
	}
      if (updated == 0)
	{
	  logger (logDEBUG) << "No feeds updated";
	}
      return true;
    }

    /*
     * Calculate score for given item
     */
    float score_item (const std::shared_ptr < Item > item)
    {
      logger (logDEBUG) << "Scoring item...";
      auto document = item_to_input (item);
      // Get per-category scores of item
      auto scores = this->classifier.score (&document);
      // Create overall score based on per-category scores
      auto score = Classifier::get_or_default (&scores, &OPENED,
					       float (0)) +
	Classifier::get_or_default (&scores, &LIKED,
				    float (0)) -
	Classifier::get_or_default (&scores, &DISLIKED, float (0));
      logger (logDEBUG) << "Scored item.";
      return score;
    }

    /*
     * Get list of few highest scoring items
     */
    std::vector < std::shared_ptr < Item >
      >items_get_unseen_limited (const size_t limit)
    {
      std::vector < std::shared_ptr < Item > >il;
      for (size_t i = 0; i < limit; i++)
	{
	  // Ensure we don't go over actual size of available items
	  if (i >= this->itemslist.size ())
	    {
	      break;
	    }
	  auto url = this->itemslist[i]->url;
	  il.push_back (this->items[url]);
	}
      return il;
    }

    /*
     * Mark item as opened
     */
    void item_mark_opened (const std::string * url)
    {
      logger (logDEBUG) << "Marking item opened: " << *url;
      std::lock_guard < std::mutex > guard (this->mutex);
      // Find item and mark opened in database
      this->items[*url]->is_opened = true;
      this->is_modified = true;
    }

    /*
     * Mark item as liked
     */
    void item_mark_liked (const std::string * url)
    {
      logger (logDEBUG) << "Marking item liked: " << *url;
      std::lock_guard < std::mutex > guard (this->mutex);
      // Find item and mark liked in database
      this->items[*url]->is_liked = true;
      this->is_modified = true;
    }

    /*
     * Mark item as dislike
     */
    void item_mark_disliked (const std::string * url)
    {
      logger (logDEBUG) << "Marking item disliked: " << *url;
      std::lock_guard < std::mutex > guard (this->mutex);
      // Find item and mark disliked in database
      this->items[*url]->is_disliked = true;
      this->is_modified = true;
    }
    // Add the given item to the classifier
    bool add_item_to_classifier (const std::string * url)
    {
      std::stringstream classes;
      if (this->items[*url]->is_disliked)
	{
	  classes << " disliked";
	}
      if (this->items[*url]->is_liked)
	{
	  classes << " liked";
	}
      if (this->items[*url]->is_opened)
	{
	  classes << " opened";
	}
      auto classstr = classes.str ();
      auto document = item_to_input (this->items[*url]);
      this->classifier.add_document (&document, &classstr);
      return true;
    }
    /*
     * Mark item as seen
     * 
     * Returns True if item is removed from list of unseen items.
     * 
     * May return False if item was previously removed
     */
    bool item_mark_seen (const std::string * url)
    {
      logger (logDEBUG) << "Marking item seen: " << *url;
      std::lock_guard < std::mutex > guard (this->mutex);
      // Find item and mark seen in database
      this->items[*url]->is_seen = true;
      this->add_item_to_classifier (url);
      // Remove item from list of unseen items
      for (auto it = this->itemslist.begin (); it != this->itemslist.end ();)
	{
	  if ((*it)->url == *url)
	    {
	      it = this->itemslist.erase (it);
	      return true;
	    }
	  else
	    {
	      ++it;
	    }
	}
      this->is_modified = true;
      return false;
    }
    std::vector < std::string > get_feeds ()
    {
      std::vector < std::string > fl;
    for (auto pair:this->feeds)
	{
	  fl.push_back (pair.first);
	}
      return fl;
    }
    bool remove_feed (const std::string * url)
    {
      logger (logDEBUG) << "Removing feed: " << *url;
      std::lock_guard < std::mutex > guard (this->mutex);
      this->feeds.erase (*url);
      return true;
    }
    //REF: https://uscilab.github.io/cereal/quickstart.html
    template < class Archive > void serialize (Archive & ar)
    {
      ar (this->items, this->feeds, this->itemslist);
    }

    bool write_to_file (const std::filesystem::path * filepath)
    {
      if (this->is_modified)
	{
	  std::lock_guard < std::mutex > guard (this->mutex);
	  logger (logDEBUG) << "Writing to file " << *filepath << "...";
	  std::ofstream os (*filepath, std::ios::binary);
	  cereal::PortableBinaryOutputArchive archive (os);

	  archive (*this);
	  this->is_modified = false;
	  return true;
	}
      else
	{
	  logger (logDEBUG) << "Skipping writing to file.";
	  return false;
	}
    }
    bool read_from_file (const std::filesystem::path * filepath)
    {
      logger (logDEBUG) << "Reading from file...";
      std::ifstream is (*filepath, std::ios::binary);

      cereal::PortableBinaryInputArchive iarchive (is);	// Create an input archive

      iarchive (*this);		// Read the data from the archive
      this->savefile = *filepath;
      this->is_modified = false;
      logger (logDEBUG) << "Read from file.";
      // Re-create classifiers using past data!
    for (auto pair:this->items)
	{
	  if (pair.second->is_seen)
	    {
	      logger (logDEBUG) << "Learning from " << pair.first;
	      this->add_item_to_classifier (&pair.first);
	    }
	}
      return true;
    }
  };

}
