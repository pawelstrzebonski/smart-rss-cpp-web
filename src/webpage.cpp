#include <sstream>

namespace webpage
{

  std::string index (const std::vector<std::string> feeds, const std::vector<std::shared_ptr<LibSmartRss::Item >> items, const uint limit)
  {
  std::stringstream html;
  html<< R"""(
<!DOCTYPE html>
<html lang=" en ">
	<head>
		<meta charset=UTF-8>
		<META name="Author" content="Pawel Strzebonski">
		<META name="Description" content="Smart - RSS Web++: A machine learning enhanced RSS feed manager.">
		<title>Smart-RSS Web ++</title>
		<style media="screen">
			iframe{
			  display: none;
			}
			body{
				background-color: lightgray;
			}
		</style>
	</head>
	<body>
		<h1>
			RSS Items
		</h1>
		<FORM action="/nextpage" method="post">
			<button type="submit" name="action">Next Page</button>
		</FORM>
		<hr>
		)""";
		int i=0;
		for(auto item:items){
			html<<"(Score: "<<item->score<<") - <b>"<<item->title<<"</b> - <i>"<<item->description<<"</i>";
			html<<"<form method=\"post\" action=\"/\">";
			html<<"<INPUT type=\"hidden\" id=\"url"<<i<<"\" name=\"url\" value=\""<<item->url<<"\" >";
			html<<R"""(
					<button type="submit" name="action" formaction="/like" formtarget="frame">Like</button>
					<button type="submit" name="action" formaction="/dislike" formtarget="frame">Dislike</button>
					<button type="submit" name="action" formaction="/open" formtarget="_blank">Open</button>
				</form>
				<hr>
			)""";
			i++;
		}
		html<<R"""(
		<FORM action="/nextpage" method="post">
			<button type="submit" name="action">Next Page</button>
		</FORM>
		<h1>
			Settings
		</h1>
		<hr>
		<FORM action="/setitemnum" method="post">
			<P>
			<LABEL for="numitems"># of items per page: </LABEL>)""";
			html<<"<INPUT type=\"number\" id=\"numitems\" name=\"num\" value="<<limit<<">";
		html<<R"""(
			<INPUT type="submit" value="Set" formtarget="_self"> <INPUT type="reset">
			</P>
		</FORM>
		<h2>
			Add a new feed:
		</h2>
		<FORM action="/add" method="post">
			<P>
			<LABEL for="urladd">Feed URL: </LABEL>
			<INPUT type="text" id="urladd" name="url">
			<LABEL for="numupdateinterval">Update interval (seconds): </LABEL>
			<INPUT type="number" id="numupdateinterval" name="num" value=3600>
			<INPUT type="submit" value="Add" formtarget="_self"> <INPUT type="reset">
			</P>
		</FORM>
		<h2>
			Remove a feed:
		</h2>
		<FORM action="/remove" method="post">
		  <LABEL for="urlremove">Feed URL:</LABEL>
		  <SELECT id="urlremove" name="url">
		  )""";
		for(auto feed:feeds){
			html<<"<OPTION value="<<feed<<">"<<feed<<"</OPTION>";
		}
		html<<R"""(
		  </SELECT>
		  <INPUT type="submit" value="Remove" formtarget="_self">
		</FORM>
		<hr>
		<FORM action="/stop" method="post">
			<button type="submit" name="action">Quit (will terminate server)</button>
		</FORM>
		<iframe name="frame"></iframe>
	</body>
</html>
)""";
  return html.str();}

}
