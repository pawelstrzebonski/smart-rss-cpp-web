{ pkgs ? import <nixpkgs> {} }:
with pkgs;

stdenv.mkDerivation rec {
	name = "smartrsscppweb";
	nativeBuildInputs = [
		pkg-config
		clang
		curl
		tinyxml-2
		httplib
		cereal
	];
	rev = "7cb951d8a1d654c5672ec810fa73b3fd279cbc23";
	src = fetchFromGitLab {
		inherit rev;
		owner = "pawelstrzebonski";
		repo = "smart-rss-cpp-web";
		sha256 = "sha256-Q3S2PBGbBlWsSVdW1cMRKMdIutFBRFPHwT67jfZpu+4=";
	};
	  buildPhase = "clang++ src/main.cpp -o smartrsscppweb `pkg-config --libs --cflags libcurl tinyxml2`";
	  installPhase = ''
		mkdir -p $out/bin
		cp smartrsscppweb $out/bin/
	  '';
	meta = with lib; {
		description = "A machine learning enhanced RSS feed manager, written in C++.";
		homepage = "https://gitlab.com/pawelstrzebonski/smart-rss-cpp-web";
		license = licenses.agpl3;
		#maintainers = with maintainers; [ pawelstrzebonski ];
		platforms = platforms.linux;
	};
}
