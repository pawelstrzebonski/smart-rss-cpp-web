CFLAGS=-Wall
LFLAGS=`pkg-config --libs --cflags libcurl tinyxml2`

buildclang:
	clang++ src/main.cpp -o smartrsscppweb $(CFLAGS) $(LFLAGS)

buildzig:
	zig c++ src/main.cpp -o smartrsscppweb $(CFLAGS) $(LFLAGS)

buildgcc:
	g++ src/main.cpp -o smartrsscppweb $(CFLAGS) $(LFLAGS)

format:
	indent src/main.cpp
	indent src/libsmartrss.cpp
	#indent src/classifier.cpp
	#indent src/webpage.cpp
	#indent src/frontend.cpp #NOTE: formatting breaks a regex line
